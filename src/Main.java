
public class Main {

	public static void main(String[] args) {
		Library lib = new Library();
		Student stu = new Student("Thanapon Chuacauwna","5610450152","D14");
		Teacher tea = new Teacher("Braum Winter","D14") ;
		Guard gua = new Guard("Darius Norxus","Security Guard") ;
		
		
		Book book01 = new Book("One piece","2008");
		Book book02 = new Book("Naruto","2012");
		Book book03 = new Book("Bleach","2010");
		Book book04 = new Book("Hunter","2003");
		Book book05 = new Book("Toriko","20013");
		Book book06 = new Book("Attack On Titan","2014");
		ReferenceBook refBook01 = new ReferenceBook("Biology","2010");
		ReferenceBook refBook02 = new ReferenceBook("Chemistry","2008");
		ReferenceBook refBook03= new ReferenceBook("ThaiSangkom","2012");
		ReferenceBook refBook04 = new ReferenceBook("Art Of Living","2013");

		
		lib.addBook("One piece",book01);
		lib.addBook("Bleach",book03);
		lib.addBook("Hunter",book04);
		lib.addRefBook("ThaiSangkom",refBook03);
		System.out.println(lib.getBookCount()); 
		lib.borrowBook(stu,"Bleach");
		lib.borrowBook(stu,"Hunter"); 
		System.out.println(lib.getBookCount()); 
		lib.returnBook(stu,"Bleach"); 
		System.out.println(lib.getBookCount()); 
		lib.borrowBook(stu,"Biology");
		System.out.println(lib.getBookCount()); 
		System.out.println(stu.getName()); 

	}
}
